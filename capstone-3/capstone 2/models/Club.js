// [ SECTION ] Dependecies and Modules
const mongoose = require('mongoose');

// [ SECTION ] Schema/Blueprint
const clubSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [ true, 'Club name is required!' ]
	},
	description : {
		type: String,
		required: [ true, 'Description is required!' ]
	},
	registration : {
		type: Number,
		required: [ true, 'Registration price is required!' ]
	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		// The "new Date()" expression that instantiates a new "date" that store the current time and date whenever a club is created in our database
		default: new Date()
	},
	enrollees : [
		{
			userId : {
				type: String,
				required: [ true, 'UserId is required!' ]
			},
			enrolledOn : {
				type: Date,
				default: new Date()
			}
		}
	]
});

// [ SECTION ] Model
module.exports = mongoose.model('Club', clubSchema);