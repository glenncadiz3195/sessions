const clubsData = [
	{
		id: "wdc001",
		name: "Zuitt Web Dev Club",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pellentesque et nibh in mattis. In facilisis eros venenatis, aliquet ex eget, euismod dolor. Aenean ut sapien porta, pretium lacus at, ullamcorper nulla. Maecenas egestas dictum tellus, a malesuada velit auctor et. Nulla facilisi. Sed sodales ut ante sed blandit. Cras non neque risus. In pulvinar, ligula eu iaculis porta, lorem ante interdum justo, sit amet suscipit mi lectus vitae massa.",
		registration: 100,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Zuitt Chess Club",
		description: "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum hendrerit feugiat neque, vel tincidunt ex bibendum sit amet. Sed maximus elit purus, eu facilisis purus convallis bibendum. Phasellus scelerisque elit et feugiat placerat. Maecenas pellentesque purus augue. Aenean convallis nec mauris vel vestibulum. Aenean nec blandit nisi, non mollis nisi. Proin sit amet erat urna.",
		registration: 200,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Zuitt Badminton Club",
		description: "Curabitur vitae enim gravida, eleifend purus eget, interdum erat. Mauris sit amet feugiat libero, ac sollicitudin dolor. Fusce sed nunc nec erat pulvinar fermentum a ac eros. Mauris vitae enim bibendum, pellentesque eros viverra, finibus sem. Mauris justo ex, pharetra in sapien pellentesque, placerat tincidunt diam. Proin pulvinar est sit amet urna blandit, a ornare nunc tempor. Cras cursus, justo in euismod finibus, metus ante posuere nunc, sit amet convallis arcu lacus non nibh.",
		registration: 150,
		onOffer: true
	}
]

export default clubsData;