import { Card, Button } from 'react-bootstrap';
import clubsData from '../data/clubsData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ClubCard({clubProp}) {

	const { _id, name, description, registration } = clubProp;

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Registration:</Card.Subtitle>
	        <Card.Text>PhP {registration}</Card.Text>
	        <Link className="btn btn-primary" to={`/clubs/${_id}`}>Details</Link>
	    </Card.Body>
	</Card>	
	)
}

ClubCard.propTypes = {
	club: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		registration: PropTypes.number.isRequired
	})
}