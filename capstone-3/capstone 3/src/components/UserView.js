import React, { useState, useEffect } from 'react';
import ClubCard from './ClubCard';
import ClubSearch from './ClubSearch';


export default function UserView({clubsData}) {

    const [clubs, setClubs] = useState([])

    useEffect(() => {
        const clubsArr = clubsData.map(club => {
            if(club.isActive === true) {
                return (
                    <ClubCard clubProp={club} key={club._id}/>
                    )
            } else {
                return null;
            }
        })

        setClubs(clubsArr)

    }, [clubsData])

    return(
        <>
            <ClubSearch />
            { clubs }
        </>
        )
}