import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditClub({club}){
	const [clubId, setClubId] = useState("");

	const [showEdit, setShowEdit] = useState(false);

	// useState for our form (modal)
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [registration, setRegistration] = useState("");

	// function for opening the edit modal

	const openEdit = (clubId) => {
		setShowEdit(true);

		// to still get the actual data from the form
		fetch(`https://capstone2-cadiz.onrender.com/clubs/${clubId}`)
		.then(res => res.json())
		.then(data => {
			setClubId(data._id);
			setName(data.name);
			setDescription(data.description);
			setRegistration(data.registration)
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName("");
		setDescription("");
		setRegistration(0);
	}

	// function to save our update
	const editClub = (e, clubId) => {
		e.preventDefault();

		fetch(`https://capstone2-cadiz.onrender.com/clubs/${clubId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				registration: registration
			})
			})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Club Successfully Updated!"
				})

				closeEdit();
			}else{
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				closeEdit();
			}

		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(club)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editClub(e, clubId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Club</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="clubName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={name} onChange={e => setName(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="clubDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="clubRegistration">
                            <Form.Label>Registration</Form.Label>
                            <Form.Control type="number" required value={registration} onChange={e => setRegistration(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
		</>

		)
}