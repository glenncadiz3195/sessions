// import { useState, useContext } from 'react';
// import Container from 'react-bootstrap/Container';
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

// import { Link, NavLink } from 'react-router-dom';
// import UserContext from '../UserContext';


// export default function AppNavbar() {

//     // State to store the user information stored in the login page.
//     // const [user, setUser] = useState(localStorage.getItem("access"));
//     // console.log(user);

//     const { user } = useContext(UserContext);

//     return(
//         <Navbar bg="primary" expand="lg" >
//             <Container fluid>
//                 <Navbar.Brand as={Link} to="/">Search Club</Navbar.Brand>
//                 <Navbar.Toggle aria-controls="basic-navbar-nav" />
//                 <Navbar.Collapse id="basic-navbar-nav">
//                     <Nav className="ms-auto">
//                         <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
//                         <Nav.Link as={NavLink} to="/clubs" exact>Clubs</Nav.Link>
//                         {(user.id !== null) ? 

//                                 user.isAdmin 
//                                 ?
//                                 <>
//                                     <Nav.Link as={Link} to="/addClub">Add Club</Nav.Link>
//                                     <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                                 </>
//                                 :
//                                 <>
//                                     <Nav.Link as={Link} to="/profile">Profile</Nav.Link>
//                                     <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
//                                 </>
//                             : 
//                                 <>
//                                     <Nav.Link as={Link} to="/login">Login</Nav.Link>
//                                     <Nav.Link as={Link} to="/register">Register</Nav.Link>
//                                 </>
//                         }
//                     </Nav>
//                 </Navbar.Collapse>
//             </Container>
//         </Navbar>
//         )
// }

import { useState, useContext } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
    const { user } = useContext(UserContext);

    const whiteTextColorStyle = { color: 'white' }; // Define the white text color style

    return (
        <Navbar bg="primary" expand="lg">
            <Container fluid>
                <Navbar.Brand as={Link} to="/" style={whiteTextColorStyle}>Search Club</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/" exact style={whiteTextColorStyle}>Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/clubs" exact style={whiteTextColorStyle}>Clubs</Nav.Link>
                        {(user.id !== null) ?
                            user.isAdmin
                                ? <>
                                    <Nav.Link as={Link} to="/addClub" style={whiteTextColorStyle}>Add Club</Nav.Link>
                                    <Nav.Link as={Link} to="/logout" style={whiteTextColorStyle}>Logout</Nav.Link>
                                </>
                                : <>
                                    <Nav.Link as={Link} to="/profile" style={whiteTextColorStyle}>Profile</Nav.Link>
                                    <Nav.Link as={Link} to="/logout" style={whiteTextColorStyle}>Logout</Nav.Link>
                                </>
                            :
                            <>
                                <Nav.Link as={Link} to="/login" style={whiteTextColorStyle}>Login</Nav.Link>
                                <Nav.Link as={Link} to="/register" style={whiteTextColorStyle}>Register</Nav.Link>
                            </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
