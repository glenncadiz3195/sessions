import React, { useState } from 'react';

const SearchByRegistration = () => {
  const [minRegistration, setMinRegistration] = useState('');
  const [maxRegistration, setMaxRegistration] = useState('');
  const [clubs, setClubs] = useState([]);

  const handleMinRegistrationChange = (e) => {
    setMinRegistration(e.target.value);
  };

  const handleMaxRegistrationChange = (e) => {
    setMaxRegistration(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ minRegistration, maxRegistratione }),
    };

    fetch('https://capstone2-cadiz.onrender.com/clubs/searchByRegistration', requestOptions)
      .then((response) => response.json())
      .then((data) => {
        setClubs(data.clubs);
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  };

  return (
    <div className="container">
      <h2>Search Clubs by Registration Range</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="minRegistration" className="form-label">
            Min Registration:
          </label>
          <input
            type="number"
            className="form-control"
            id="minRegistration"
            value={minRegistration}
            onChange={handleMinRegistrationChange}
          />
        </div>
        <div className="mb-3">
          <label htmlFor="maxRegistration" className="form-label">
            Max Registration:
          </label>
          <input
            type="number"
            className="form-control"
            id="maxRegistratione"
            value={maxRegistration}
            onChange={handleMaxRegistrationChange}
          />
        </div>
        <button type="submit" className="btn btn-primary">
          Search
        </button>
      </form>
      <h3>Search Results:</h3>
      <ul>
        {clubs.map((club) => (
          <li key={club.id}>{club.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default SearchByRegistration;