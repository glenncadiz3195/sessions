import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Footer from '../components/Footer';

export default function Home() {

    const data = {
    	/* textual contents*/
        title: "Search Club",
        content: "Looking for an interest group? You can find it here!",
        /* buttons */
        destination: "/clubs",
        label: "Join now!"
    }

    return (
        <>
            <Highlights />
            <Banner data={data}/>
            <Footer />
        </>
    )
}