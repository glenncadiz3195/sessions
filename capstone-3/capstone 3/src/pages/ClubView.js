import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ClubView(){

	const {user} = useContext(UserContext);

	const {clubId} = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [registration, setRegistration] = useState(0);

	const enroll = (clubId) => {
		fetch(`https://capstone2-cadiz.onrender.com/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				clubId: clubId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.message === false){
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
				
			}else{
				Swal.fire({
					title: "Successfully Registered!",
					icon: "success",
					text: "You have successfully joined this club."
				})

				navigate("/clubs");
			}
		})
	}

	useEffect(() => {
		console.log(clubId);

		fetch(`https://capstone2-cadiz.onrender.com/clubs/${clubId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setRegistration(data.registration);
		})

	}, [clubId])


	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Registration:</Card.Subtitle>
                            <Card.Text>PhP {registration}</Card.Text>
                            {/* <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>8 am - 5 pm</Card.Text> */}

                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => enroll(clubId)}>Register</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login to Register</Link>
                            }

                            
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}