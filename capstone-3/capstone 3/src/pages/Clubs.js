import { useEffect, useState, useContext } from 'react';
import ClubCard from '../components/ClubCard';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from './AdminView';

export default function Clubs() {

    const { user } = useContext(UserContext);

    const [clubs, setClubs] = useState([]);
    

    const fetchData = () => {
        fetch(`https://capstone2-cadiz.onrender.com/clubs/all`)
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            // Sets the "clubs" state to map the data retrieved from the fetch request into several "ClubCard" components
            setClubs(data);

        });
    }


    // Retrieves the clubs from the database upon initial render of the "Clubs" component
    useEffect(() => {

        fetchData()

    }, []);


    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView clubsData={clubs} fetchData={fetchData} />

                    :

                    <UserView clubsData={clubs} />

            }
        </>
    )
}