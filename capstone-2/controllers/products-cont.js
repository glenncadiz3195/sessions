const Product = require('../models/Products-model.js');

module.exports.addProduct = (req, res) => {
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    });

    return newProduct.save().then((product, error) => {
        if(error){
            return res.send (false)
        }else{
            return res.send ("New pet added in store.")
        }
    })
    .catch(error => res.send(error));
}

module.exports.getAllProducts = (req, res) => {
    return Product.find().then(result => {
        if(result.lenght === 0){
            return res.send("No Pets Available.");
        }else{
            return res.send(result);
        }
    })
}

module.exports.getAllActive = (req, res) => {
    return Product.find({isActive : true}).then(result => {
        if(result.length === 0){
            return res.send("Store is Empty!")
        }else{
            return res.send(result);
        }
    })
}

module.exports.getProduct = (req, res) => {
    return Product.findById(req.params.productId).then(result => {
        if(result === 0){
			return res.send("No pet found.")
		}else{
			if(result.isActive === false){
				return res.send("Pet is not available.");
			}else{
				return res.send(result);
			}
		}
    })
    .catch(error => res.send("Please enter a correct pet ID"))
}


module.exports.updateProduct = (req, res) => {
    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {
        if(error){
            return res.send(false);
        }else{
            return res.send("Pet updated");
        }
    })
}


module.exports.archiveProduct = (req, res) => {
    let activeProduct = {
        isActive: false
    }

    return Product.findByIdAndUpdate(req.params.productId, activeProduct).then((product, error) => {
        if(error){
            return res.send(false);
        }else{
            return res.send("Pet successfully archived.");
        }
    })
    .catch(error => res.send(error))
}


module.exports.activateProduct = (req, res) => {
    let deletedProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, deletedProduct).then((product, error) => {
        if(error){
            return res.send(false);
        }else{
            return res.send("Pet activated.");
        }
    })
    .catch(error => res.send(error))
}