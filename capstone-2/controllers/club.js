//[SECTION] Dependencies and Modules
    const Club = require("../models/Club");
    const User = require("../models/User");


//[SECTION] Create a new club

    module.exports.addClub = (req, res) => {
        let newClub = new Club({
                name : req.body.name,
                description : req.body.description,
                registration : req.body.registration
            });

        return newClub.save().then((club, error) => {
            // Club creation successful
            if (error) {
                return res.send(false);

            // Club creation failed
            } else {
                return res.send(true);
            }
        })
        .catch(err => res.send(err))
    }


//[SECTION] Retrieve all clubs
    module.exports.getAllClubs = (req, res) => {
        return Club.find({}).then(result => {
            console.log(result);
            return res.send(result);
        })
        .catch(err => res.send(err))
    };


//[SECTION] Retrieve all ACTIVE clubs
    module.exports.getAllActive = (req, res) => {
        return Club.find({ isActive : true }).then(result => {
            return res.send(result);
        })
        .catch(err => res.send(err))
    };

//[SECTION] Retrieving a specific club
    module.exports.getClub = (req, res) => {
        return Club.findById(req.params.clubId).then(result => {
            return res.send(result);
        })
        .catch(err => res.send(err))
    };


//[SECTION] Update a club
    module.exports.updateClub = (req, res) => {
            // Specify the fields/properties of the document to be updated
            let updatedClub = {
                name : req.body.name,
                description : req.body.description,
                price : req.body.price
            };

            // Syntax
                // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Club.findByIdAndUpdate(req.params.clubId, updatedClub).then((club, error) => {

                // Club not updated
                if (error) {
                    return res.send(false);

                // Club updated successfully
                } else {                
                    return res.send(true);
                }
            })
            .catch(err => res.send(err))
        };


//SECTION] Archive a club
    module.exports.archiveClub = (req, res) => {

        let updateActiveField = {
            isActive: false
        }

        return Club.findByIdAndUpdate(req.params.clubId, updateActiveField)
        .then((club, error) => {

            //club archived successfully
            if(error){
                return res.send(false)

            // failed
            } else {
                return res.send(true)
            }
        })
        .catch(err => res.send(err))

    };


//[SECTION] Activate a club
    module.exports.activateClub = (req, res) => {

        let updateActiveField = {
            isActive: true
        }

        return Club.findByIdAndUpdate(req.params.clubId, updateActiveField)
            .then((club, error) => {

            //club archived successfully
            if(error){
                return res.send(false)

            // failed
            } else {
                return res.send(true)
                }
        })
        .catch(err => res.send(err))

    };

//ChatGPT Generated Code

module.exports.searchClubsByName = async (req, res) => {
    try {
      const { clubName } = req.body;
  
      // Use a regular expression to perform a case-insensitive search
      const clubs = await Club.find({
        name: { $regex: clubName, $options: 'i' }
      });
  
      res.json(clubs);
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
};

// Controller to get the list of emails of users enrolled in a club
//Solution
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const clubId = req.params.clubId;

  try {
    // Find the club by clubId
    const club = await Club.findById(clubId);

    if (!club) {
      return res.status(404).json({ message: 'Club not found' });
    }

    // Get the userIds of enrolled users from the club
    const userIds = club.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } });

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email);

    res.status(200).json({ emails });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};

    exports.searchClubsByPriceRange = async (req, res) => {
        try {
          const { minPrice, maxPrice } = req.body;
      
          // Find clubs within the price range
          const clubs = await Club.find({
            price: { $gte: minPrice, $lte: maxPrice }
          });
      
          res.status(200).json({ clubs });
        } catch (error) {
          res.status(500).json({ error: 'An error occurred while searching for clubs' });
        }
      };