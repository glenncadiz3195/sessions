// [ SECTION ] Dependecies and Modules
const User = require("../models/User-model");
const bcrypt = require('bcrypt');
const Product = require("../models/Products-model")
const auth = require("../auth.js");


// user registration
module.exports.registerUser = (reqBody) => {
	console.log(reqBody);
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error){
			return "Please check info provide."
		} else {
			return "User registered!"
		}
	})
	.catch(err => err);
};

// user authentication
module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {
		console.log(result);
		if(result === null){
			return false; // the email doesn't exist in our DB
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);

			if(isPasswordCorrect){
				//return res.send({ access: auth.createAccessToken(result)})
				return { access: auth.createAccessToken(result)}
			} else {
				return false; // Passwords do not match
			}
		}
	})
}


// Retrieve user details
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "*****";
		return res.send(result);
	})
	.catch(error => error)
}

// buy pet
module.exports.buy = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.productId);

	if (req.user.isAdmin) {
		return res.send("Restricted Action.");
	}

	let isUserUpdated = await User.findById(req.user.id).then(async (user) => {
		let productId = req.body.productId;
		let product = await Product.findById(productId).then(result => {
			return result
		})
	
		if (!product) {
			return res.send("Product not found.");
		}
	
		const quantity = req.body.quantity; //input quantity
	
		// Calculate the total amount based on the product's price and quantity
		const totalAmount = product.price * quantity;
	
		const bought = {
			productId: product._id,
			productName: product.name,
			quantity: quantity,
			price : product.price,
			totalAmount : totalAmount
		};
		
	let listOfProducts = user.orderedProduct;

	let filteredProductId = [];

	listOfProducts.forEach((result) => {
		filteredProductId.push(result);
	});
	
	console.log(filteredProductId);

	let productDuplicate = filteredProductId.includes(req.body.productId);

	console.log("product duplicate: " + productDuplicate);

	if (productDuplicate) {
		return res.send("You already have this pet.");
	}

	user.orderedProduct.push(bought);

	return user.save()
	.then(user => true)
	.catch(error => ({ message: error.message }));
	})

	if (isUserUpdated !== true) {
		return res.json({ message: isUserUpdated.message });
	}

	let isProductUpdated = await Product.findById(req.body.productId).then(product => {
	let buyer = {
		userId: req.user.id
	}

	product.userOrders.push(buyer);

	return product.save()
		.then(product => true)
		.catch(error => ({ message: error.message }));
	})

	if (isProductUpdated !== true) {
		return res.json({ message: isProductUpdated.message }); 
	}

	if (isUserUpdated && isProductUpdated) {
		return res.send("CONGRATULATIONS! You purchased a pet!");
	}
}


// view orders
module.exports.getPurchased = (req, res) => {
	User.findById(req.user.id).then(result => {
		if(result.orderedProduct == null || result.orderedProduct.length < 1){
			return res.send("No orders made yet.")
		}else{
			res.send(result.orderedProduct);
		}
	}).catch(error => res.send(error));
}
