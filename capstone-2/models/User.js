//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    // The "enrollments" property/field will be an array of objects containing the club IDs, the date and time that the user enrolled to the club and the status that indicates if the user is currently enrolled to a club
    enrollments : [
        {
            clubId : {
                type : String,
                required : [true, "Club ID is required"]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Registered"
            }
        }
    ]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);