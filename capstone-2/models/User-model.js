//[SECTION] Modules and Dependencies
const mongoose = require("mongoose");

//[SECTION] Schema/Blueprint
const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    mobileNo : {
        type : String, 
        required : [true, "Mobile No is required"]
    },
    orderedProduct : [
        {
            productId: {
                type : String,
                required : [true, "Product ID is required"]
            },
            productName: {
                type : String,
                required : [true, "Product Name is required"]
            },
            quantity: {
                type : Number,
                required : [true, "Quanity is required"]
            },
            price : {
                type : Number,
                required : [true, "Price is required"]
            },
            totalAmount : {
                type : Number,
                required : [true, "Total Amount is required"]
            },
            purchasedOn : {
                type : Date,
                default : new Date()
            }
        }
    ]
})

//[SECTION] Model
module.exports = mongoose.model("User", userSchema);