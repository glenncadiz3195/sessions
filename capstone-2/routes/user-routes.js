// [ SECTION ] Dependencies and Modules
const express = require('express');
const auth = require("../auth.js")
const {verify, verifyAdmin} = auth;
const router = express.Router();
const userController = require('../controllers/user-cont.js');

// [ SECTION ] Routing Component

// user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// get user details
router.get("/details", verify, userController.getProfile);

// buy pet
router.post("/buy", verify, userController.buy);

// view orders
router.get("/purchased", verify, userController.getPurchased);


// [ SECTION ] Export Route System
module.exports = router;