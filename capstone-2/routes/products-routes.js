const express = require('express');
const productsController = require("../controllers/products-cont.js")

const auth = require("../auth.js");

//destructuring of verify and verifyAdmin

const {verify, verifyAdmin} = auth;

const router = express.Router();

// Create a product
router.post("/", verify, verifyAdmin, productsController.addProduct);

// Get all products
router.get("/all", productsController.getAllProducts);

// Get all "active" products
router.get("/active", productsController.getAllActive);

// Get 1 specific product using its ID
router.get("/:productId", productsController.getProduct);

// Updating a Product (Admin Only)
router.put("/:productId", verify, verifyAdmin, productsController.updateProduct);

// Archive a product
router.put("/:productId/archive", verify, verifyAdmin, productsController.archiveProduct);

// Activating a course
router.put("/:productId/activate", verify, verifyAdmin, productsController.activateProduct);

// [SECTION] Export Route System
module.exports = router;


