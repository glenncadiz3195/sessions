//[SECTION] Dependencies and Modules
    const express = require('express');
    const clubController = require("../controllers/club");
    const auth = require("../auth") 

    const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
    const router = express.Router();


//[ACTIVITY] create a club POST
    router.post("/", verify, verifyAdmin, clubController.addClub);


//[SECTION] Route for retrieving all the clubs 
    router.get("/all", clubController.getAllClubs);

//[SECTION] Route for retrieving all the ACTIVE clubs for all users
    // Middleware for verifying JWT is not required because users who aren't logged in should also be able to view the clubs
    router.get("/", clubController.getAllActive);


//[SECTION] Route for Search Club by Name
    router.post('/searchByName', clubController.searchClubsByName); 

//[ACTIVITY] Search Clubs By Price Range
    router.post('/searchByPrice', clubController.searchClubsByPriceRange);

//[SECTION] Route for retrieving a specific club
    router.get("/:clubId", clubController.getClub);

//[SECTION] Route for updating a club (Admin)
    router.put("/:clubId", verify, verifyAdmin, clubController.updateClub);

// [SECTION]Route to get the emails of users enrolled in a club

// Solution
    router.get('/:clubId/enrolled-users', clubController.getEmailsOfEnrolledUsers);

//[ACTIVITY] Route to archiving a club (Admin)
    router.put("/:clubId/archive", verify, verifyAdmin, clubController.archiveClub);

//[ACTIVITY] Route to activating a club (Admin)
    router.put("/:clubId/activate", verify, verifyAdmin, clubController.activateClub);





// Allows us to export the "router" object that will be accessed in our "index.js" file
        module.exports = router;
