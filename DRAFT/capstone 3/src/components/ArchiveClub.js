import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveClub({club, isActive, fetchData}) {

    const archiveToggle = (clubId) => {
        fetch(`https://capstone2-cadiz.onrender.com/clubs/${clubId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Club successfully archived'
                })
                fetchData();

            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }


        const activateToggle = (clubId) => {
        fetch(`https://capstone2-cadiz.onrender.com/clubs/${clubId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })

        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data === true) {
                Swal.fire({
                    title: 'Success',
                    icon: 'success',
                    text: 'Club successfully retrieved'
                })
                fetchData();
            }else {
                Swal.fire({
                    title: 'Something Went Wrong',
                    icon: 'Error',
                    text: 'Please Try again'
                })
                fetchData();
            }


        })
    }
 

    return(
        <>
            {isActive ?

                <Button variant="danger" size="sm" onClick={() => archiveToggle(club)}>Archive</Button>

                :

                <Button variant="success" size="sm" onClick={() => activateToggle(club)}>Activate</Button>

            }
        </>

        )
}