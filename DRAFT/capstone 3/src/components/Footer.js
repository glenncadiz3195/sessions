import { Row, Col, Container } from 'react-bootstrap';
import React from 'react';

const Footer = () => {
    return (
        <footer className="bg-primary">
            <Container fluid>
                <Row>
                    <h4 className="p-3 text-center text-light">Contact Us!</h4>
                    <Col className="p-1 text-center text-light">
                        <p>Email: searchclubofficial@mail.com</p>
                    </Col>
                    <Col className="p-1 text-center text-light">
                        <p>Mobile: 09123456789</p>
                    </Col>
                    <Col className="p-1 text-center text-light">
                        <p>Facebook: @searchclubph</p>
                    </Col>
                    <Col className="p-1 text-center text-light">
                        <p>Instagram: @searchclubph</p>
                    </Col>
                </Row>
            </Container>
        </footer>
    );
};

export default Footer;
