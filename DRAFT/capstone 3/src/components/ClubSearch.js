import React, { useState } from 'react';
import ClubCard from './ClubCard';
const ClubSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    try {
      const response = await fetch('https://capstone2-cadiz.onrender.com/clubs/searchByName', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ clubName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
    } catch (error) {
      console.error('Error searching for clubs:', error);
    }
  };

  return (
    <div className='pt-5 container bg-p'>
      <h2>Club Search</h2>
      <div className="form-group">
        <label htmlFor="clubName">Club Name:</label>
        <input
          type="text"
          id="clubName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(club => (
        //   <li key={club._id}>{club.name}</li>
          <div className="p-3 bg-success">
        <ClubCard clubProp={club} key={club._id}/>
        </div>
        ))}
      </ul>
    </div>
  );
};

export default ClubSearch;
