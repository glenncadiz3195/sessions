
import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';

import EditClub from '../components/EditClub';
import ArchiveClub from '../components/ArchiveClub';


export default function AdminView({ clubsData, fetchData }) {


    const [clubs, setClubs] = useState([])

    useEffect(() => {
        const clubsArr = clubsData.map(club => {
            return (
                <tr key={club._id}>
                    <td>{club._id}</td>
                    <td>{club.name}</td>
                    <td>{club.description}</td>
                    <td>{club.registration}</td>
                    <td className={club.isActive ? "text-success" : "text-danger"}>
                        {club.isActive ? "Available" : "Unavailable"}
                    </td>
                    <td> <EditClub club={club._id} fetchData={fetchData} /> </td>
                     <td><ArchiveClub club={club._id} isActive={club.isActive} fetchData={fetchData}/></td>
                </tr>
                )
        })

        setClubs(clubsArr)

    }, [clubsData])


    return(
        <>
            <h1 className="text-center my-4"> Welcome Admin!</h1>
            
            <Table striped bordered hover responsive>
                <thead>
                    <tr className="text-center">
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Registration</th>
                        <th>Availability</th>
                        <th colSpan="2">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    {clubs}
                </tbody>
            </Table>    
        </>

        )
}