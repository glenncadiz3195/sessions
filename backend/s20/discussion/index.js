// console.log("Hello World!");

// [SECTION] Arithmetic Operators

let x = 26;
let y = 5;

let sum =  x + y;
console.log("Results from addtion operator: " + sum);

let difference =  x - y;
console.log("Results from substraction operator: " + difference);

let product =  x * y;
console.log("Results from multiplication operator: " + product);

let quotient =  x / y;
console.log("Results from division operator: " + quotient);

// modulo % - it displays the remainder
let remainder =  x % y;
console.log("Results from modulo operator: " + remainder);

// [SECTION] Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

assignmentNumber = assignmentNumber + 2;
console.log("Addition assignment: " + assignmentNumber);

// Shortcut Method for Assignment Method is +=

assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Subtraction assignment: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Multiplication assignment: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Division assignment: " + assignmentNumber);

// Multiple Operators and Parentheses
/*
            - When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6
*/

let mdas = 1+2-3*4/5;
console.log("Result from mdas operation: " + mdas);

/*
            - By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
            - The operations were done in the following order:
                1. 4 / 5 = 0.8
                2. 2 - 3 = -1
                3. -1 * 0.8 = -0.8
                4. 1 + -.08 = .2
*/
let pemdas = 1 + (2 - 3) * (4/5);
console.log("Results from pemdas operation: " + pemdas);

// Increment and Decrement
// Incrementation -> ++
// Decrementation -> --

let z = 1; 

// pre-increment = +1 on the original value
let increment = ++z;
console.log("pre-increment: " + increment);
console.log("pre-increment: " + z);

//  post-increment = it will get the original value
increment = z++;
console.log("post-increment: " + increment);

let myNum1 = 1;
let myNum2 = 1;

let preIncrement = ++myNum1;
preIncrement = ++myNum1;
console.log(preIncrement);

let postIncrement = myNum2++;
postIncrement = myNum2++;
console.log(postIncrement);

let myNumA = 5;
let myNumB = 5;

let preDecrement = --myNumA; // pre-decrement is minus 1
preDecrement = --myNumA;
console.log(preDecrement);

let postDecrement = myNumB--; //gitawag ang original value
postDecrement = myNumB--;
console.log(postDecrement)

// [SECTION] Type Coercion
// Automatic or implicit conversion of value from one data type to another

let myNum3 = "10";
let myNum4 = 12;

let coercion = myNum3 + myNum4;
console.log(coercion);
console.log(typeof coercion);

let myNum5 = true + 1; //true is equal to 1
console.log(myNum5);

let myNum6 = false + 1; //false is equal to 0
console.log(myNum6);

// [SECTION] Comparison Operator

let juan = "juan";

// Equality Operator (==)
console.log(juan == "juan");
console.log(1 == "juan");
console.log(false == 0);

// Inequality Operator (!=)
// ! -> means NOT
console.log(juan != "juan");
console.log(1 != "juan");
console.log(false != 0);

// Strict Equality Operator (===)
// Same data type, content or value
console.log(1 === 1);
console.log(false === 0); //false bec false is boolean

// Strict Inequality (!==)
console.log(1 !== 1);
console.log(false !== 0);

// [SECTION] Relational Operator
// comparison operations whether one value is greater or less than to the other value.\

let a = 40;
let b = 50;

let example1 = a > b;
console.log(example1);

let example2 = a < b;
console.log(example2);

let example3 = a >= b;
console.log(example3);

let example4 = a <= b;
console.log(example4);

// [SECTION] Logical Operator

// Logical "AND" (&& - Double Ampersand)

let isLegalAge = true;
let isRegistered = true;

// All conditions should be true
let allRequirementsMet = isLegalAge && isRegistered;

console.log("Logical AND: " + allRequirementsMet);

// Logical "OR" (|| - Double Pipe)

let isLegalAge2 = true;
let isRegistered2 = false;

let someRequirementsMet = isLegalAge2 || isRegistered; //atleast 1 true it will be true
console.log(someRequirementsMet);

// Logical "NOT" (! - Exclamation Point)

let isRegistered3 = false;

let someRequirementsNotMet = !isRegistered3;
console.log("Result from logical NOT: " + someRequirementsNotMet);

