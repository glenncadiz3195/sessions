let http = require("http");

let port = 4000;

let app = http.createServer(function(request, response){

    if(request.url == "/" && request.method == "GET"){

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to Booking System");
    }
    if(request.url == "/profile" && request.method == "GET"){

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Welcome to your profile!");
    }
    if(request.url == "/courses" && request.method == "GET"){

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Here’s our courses available");
    }

    // The method "POST"
    if(request.url == "/addcourse" && request.method == "POST"){

        response.writeHead(200, {"Content-Type": "text/plain"});
        response.end("Add a course to our resources");
    }

    // The method "PUT"
    if(request.url == "/updatecourse" && request.method == "PUT"){
        
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Update a course to our resources");
    }

    // The method "DELETE"
    if(request.url == "/archivecourses" && request.method == "DELETE"){
        
        response.writeHead(200, {"Content-Type": "text/plain"})
        response.end("Archive courses to our resources");
    }

})

// Inform us if the server is running, by printing our message:
// First Argument, the port number to assign the server
// Second Argument, the callback/function to run when the server is running
app.listen(port, () => console.log("Server is running at localhost:4000"));