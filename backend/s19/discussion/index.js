console.log("Helluw World?")

// [SECTION] Syntax, Statement and Comments
    // JS Statement usually end with semicolon(;)

    // Comments:

    // There are two typees of comments:
        //1. The single-line comment denoted by //
        //2. The multiple-line comment denoted by /**/, double slash and asterisk in the middle

// [SECTION] Variables
    //  It is used to contain data
    // ANy information that is used by an application is stored in what we call a "memory"

    // Declaring Variables

    // Declaring Variables - tells our devices that a variable name is created.

    let myVariableNumber; // A naming convension
    console.log("myVariableNumber");
    console.log(myVariableNumber);
    // let MyVariableNumber
    // let my-variable-number
    // let my_variable_number

    // Syntax
        //let/const/var - variableName

    // Declaring and Initializing Variables

    let productName = "desktop computer";
    console.log(productName);

    let productPrice1 = "18999";
    let productPrice = 18999;
    console.log(productPrice1);
    console.log(productPrice);

    // const, impossible to reassign
    const interest = 3.539;
    
    // Reassigning variable values - remove let

    productName = "Laptop";
    console.log(productName);

    // interest = 4.239  //Will return an error
    // console.log(interest)

    //Reassigning Variables vs Initializing Variables

    //Always declare the variable -rule of thumb

    myVariableNumber = 2023;
    console.log(myVariableNumber);

    // Multiple Variable Declaration

    let productCode = 'DC017'; //single quotation is just the same with double quotation
    const productBand = 'Dell';

    console.log(productCode, productBand);

// [SECTION] Data Types

    // Strings

    let country = "Philippines";
    let province = 'Metro Manila';

    //Concatenation Strings
    let fullAddress = province + ", " + country;
    console.log(fullAddress);


    let greeting = "I live in the" + country;
    console.log(greeting);

    let mailAddress = 'Metro Manila \n\nPhilippines';
    console.log(mailAddress);

    let message = "John's employees went home early";
    console.log(message);

    message = 'John\'s employees went home early again!';
    console.log(message)

// Numbers
let headcount = 26;
console.log(headcount);

//Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

//Exponential Notation
//This is not related to, 2 raise to the power of 10
let planetDistance = 2e10; // e means the number of 0
let planetDistance1 = 20000000000;
console.log(planetDistance)
console.log(planetDistance1)

//Combination text and strings
console.log("John's grade last quarter is " + grade);


//Boolean
//Returns true or false

let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);


// Arrays - always named in plural form; always uses square brackets []
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(grades[0]); //only 98.7 will display since it is the only one being
console.log(grades[1]);
console.log(grades[2]);
console.log(grades[3]);
console.log(grades[4]);

// Different data types

let details = ["John", "Smith", 32, true];
console.log(details);
console.log(details[0]);
console.log(details[1]);
console.log(details[2]);
console.log(details[3]);
console.log(details[4]);

//Objects - always use curly braces {}
//Composed of "key/value pair"
let person = {
    fullName: "Juan Dela Cruz",
    age: 35,
    isMarried: false,
    contact: ["0912345789", 0912345789],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
};

console.log(person);
console.log(person.fullName); //calling fullName only
console.log(person.age);
console.log(person.isMarried);
//We only use [] to call the index number of the data from the array
console.log(person.contact[0]);
console.log(person.contact[1]);
console.log(person.address);
console.log(person.address.houseNumber); //calling houseNumber only

let array = [
    ["hey", "hey1", "hey2"],
    ["hey", "hey1", "hey2"],
    ["hey", "hey1", "hey2"],
];

console.log(array);

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrading: 94.6
};

console.log(myGrades);

//Type of Operator

console.log(typeof myGrades);
console.log(typeof array);
console.log(typeof greeting);

//Null

let spouse = null;
let myNumber = 0;
let myString = '';

console.log(spouse);
console.log(myNumber);
console.log(myString);

//Undefined
let fullName;
console.log(fullName);