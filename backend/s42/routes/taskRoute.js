const express = require("express");
// Allows access to HTTP Methods and middlewares
const router = express.Router();
const taskController = require("../controllers/taskController");

// Get all tasks
router.get("/", (req,res) => {
    taskController.getAllTask().then(resultFromController => res.send(resultFromController));
});

// Create task
router.post("/", (req,res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})


// Delete a task using wildcard on params
// ":" -> wildcard
router.delete("/:id", (req, res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Update a task
router.put("/:id", (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})



// ACTIVITY START

//Get specific task
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//Changing status
router.put("/:id/:stt", (req, res) => {
	taskController.updateTaskStatus(req.params).then(resultFromController => res.send(resultFromController));
})

// ACTIVITY END


module.exports = router;