// console.log("Hello World");


//Objective 1
//Add code here
//Note: function name is numberLooper


let number = Number(prompt("Please enter a number"))
console.log("The number you provided is " + number);
for (; number >= 0 ; number--){
    if (number <= 50){
        console.log();
        break;
    }if(number % 10 === 0){
        console.log("The number is divisible by 10. Skipping the number.")
        continue;
    }else if(number % 5 === 0){
        console.log(number);
    };
}


//Objective 2
let string = 'supercalifragilisticexpialidocious';
console.log(string);
let filteredString = '';

//Add code here
for (let i=0; i < string.length; i++){
    if (
        string[i].toLowerCase() == "a" ||
        string[i].toLowerCase() == "i" ||
        string[i].toLowerCase() == "u" ||
        string[i].toLowerCase() == "e" ||
        string[i].toLowerCase() == "o")
    {continue;
    }else{
        filteredString += string[i];
    }
}
console.log(filteredString);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
        numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
    }
} catch(err){

}



// // SOLUTION FROM SIR START HERE

// // console.log("Hello World");


// function numberLooper(number){
            
//     //Create a message variable that will contain the messages after the loop has finished.
//     let message;

//     // Creates a loop that will use the number provided by the user and count down to 0
//     for (let count = number; count >= 0; count--) {

//         // If the value provided is less than or equal to 50, terminate the loop
//         if (count <= 500) {

//             message = "The current value is at " + count + ". Terminating the loop.";
//             console.log(message);

//             break;

//         // If the value is divisible by 10, skip printing the number
//         } else if (count % 10 === 0) {

//             console.log("The number is divisible by 10. Skipping the number.");
//             continue;

//         // If the value is divisible by 5, print the number
//         } else if (count % 5 === 0) { 

//             console.log(count);

//         }

//     }

//     return message;

// }



// let string = 'supercalifragilisticexpialidocious';
// console.log(string);
// let filteredString = '';

// console.log(string.length);

// // Creates a loop that will iterate through the whole string
// for (let i=0; i < string.length; i++) {

//     // Check what is the starting value of the loop
//     // console.log(string[i]);
    
//     // If the current letter being evaluated is a vowel
//     if (
//         string[i].toLowerCase() == 'a' ||
//         string[i].toLowerCase() == 'e' ||
//         string[i].toLowerCase() == 'i' ||
//         string[i].toLowerCase() == 'o' ||
//         string[i].toLowerCase() == 'u'
//     ) {

//         // Continue the loop to the next letter/character in the sequence
//         continue;

//     // If the current letter being evaluated is not a vowel
//     } else {

//         // Add the letter to a different variable
//         filteredString += string[i];

//         // filteredString = filteredString + string[i];

//     }

// }

// // After the loop is complete, print the filtered string without the vowels
// console.log(filteredString);


// //Do not modify
// //For exporting to test.js
// try{
//     module.exports = {

//         filteredString: typeof filteredString !== 'undefined' ? filteredString : null,
//         numberLooper: typeof numberLooper !== 'undefined' ? numberLooper : null
//     }
// } catch(err){

// }