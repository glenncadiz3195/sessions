// [SECTION] Dependencies and Modules
const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.js');
const auth = require("../auth.js");

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
// const router = express.Router();


// check email routes
router.post('/checkEmail', (req,res) => {
    userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// user registration routes
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
router.post('/login', userController.loginUser)


// ACTIVITY S44 START

/* MY ANSWER
router.post('/getProfile', (req, res) => {
	userController.getProfile(req.body.id).then((resultFromController) => {
        if(resultFromController == null){
            res.send("ID not found!");
        }else{
            resultFromController.password = "";
            res.send(resultFromController);
        }
    });
});
*/

// retrieve user details  --> ANSWER OF SIR ROM
/* router.post("/details", verify, (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
}) */

// ACTIVITY S44 ENDS

router.get("/details", verify, userController.getProfile);

// Enroll user to a course
router.post("/enroll", verify, userController.enroll);

// [SECTION] Export Route System
module.exports = router;

// ACTIVTY s47
router.get('/getEnrollments', verify, userController.getEnrollments);

/*
//Updated Routes
router.post('/enroll', verify, userController.checkIfUserIsAlreadyEnrolledToCourse, userController.enroll);
*/

//Reset Password
router.put("/reset-password", verify, userController.resetPassword);