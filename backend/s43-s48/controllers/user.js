// [SECTION] Dependencies and Modules
const User = require('../models/User');
const Course = require("../models/Course")
const bcrypt = require('bcrypt');

const auth = require("../auth")

//check email exists
/*
    Steps:
    1. Use mongoose 'find' method to find duplicate emails
    2. Use the 'then' method to send a response back to the frontend application based on the result of the "find" method.
*/
module.exports.checkEmailExist = (reqBody) => {
    console.log(reqBody);
    return User.find({ email:reqBody.email }).then(result => {
        console.log(result);
        if(result.length > 0){
            return true
        }else{
            return false
        };
    });
};

// user registration
/*
    Steps:
    1. Create a new User object using the mongoose model and the information from the request body
*/
module.exports.registerUser = (reqBody) => {
    console.log(reqBody);
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10) 
    });

    return newUser.save().then((user, error) => {
        if(error){
            return false
        }else{
            return false
        }
    })
    .catch(err => err);
};

//user authentication
module.exports.loginUser = (req, res) => {
    
    return User.findOne({ email: req.body.email}).then(result => {
        console.log(result);
        if(result == null){
            return false // the email doesn't exist in our DB
        }else{

            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

            console.log(isPasswordCorrect);

            if(isPasswordCorrect){
                return res.send({ access: auth.createAccessToken(result)})
            }else{
                return res.send(false); // Password do not match
            }
        }
    })
    .catch(err => res.send(err))
}


// ACTIVITY S44 START

/* MY ANSWER
module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody).then((result, error) => {
		if(error){
			return null;
		}else{
			return result;
		}
	});
};
*/

// Retrieve user details  --> ANSWER OF SIR ROM
module.exports.getProfile = (req, res) => {
	return User.findById(req.user.id).then(result => {
		result.password = "THIS IS A PASSWORD";
		return res.send(result);
	})
	.catch(error => error)
}


module.exports.enroll = async (req, res) => {
	console.log(req.user.id);
	console.log(req.body.courseId);

	if(req.user.isAdmin){
		return res.send("You cannot do this action.")
	}

	let isUserUpdated = await User.findById(req.user.id).then(user => {
		let newEnrollment = {
			courseId: req.body.courseId,
			courseName: req.body.courseName,
			courseDescription: req.body.courseDescription,
			coursePrice: req.body.coursePrice
		}

		user.enrollments.push(newEnrollment);

		return user.save().then(user => true).catch(error => res.send(error));
	})

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(error => res.send(error));
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated});
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send("You are now enrolled to a course. Thank you!");
	}
}


module.exports.getEnrollments = (req, res) => {
	User.findById(req.user.id).then(result => {
		if(result.enrollments === [] || result.enrollments == null){
			return res.send("You are not enrolled to any courses.")
		}else{
			res.send(result.enrollments);
		}
	}).catch(error => res.send(error));
}


module.exports.resetPassword = async (req, res) => {
	try{
		const newPassword = req.body.newPassword;
		const userId = req.user.id;

		// Hashing the new password
		const hashedPassword = await bcrypt.hash(newPassword, 10);

		await User.findByIdAndUpdate(userId, {password: hashedPassword});

		res.status(200).json({message: "Password reset successfully"});
	}catch(error){
		res.status(500).json({message: "Internal Server Error"});
	}
}


/*
//Activity Controller s47 Start
module.exports.getEnrollments = (req, res) => {
	return User.findById(req.user.id).then((result) => {
		if(result.enrollments.length < 1){
			return res.send("No Courses Enrolled.");
		}else{
			return res.send(result.enrollments);
		}
	});
}
//Activity s47 Ends
*/


/*
//Updated enroll controller
module.exports.enroll = async (request, response) => {
	console.log(request.user.id);
	console.log(request.body.courseID);

	if(request.user.isAdmin){
		return response.send("Admin isn't allowed to enroll.");
	}

	let isUserUpdated = await User.findById(request.user.id).then((user_result, error) => {
		console.log("1");
		return Course.findById(request.body.courseID).then((result) => {
			console.log("2");
		
			let newEnrollment = {
				courseId: request.body.courseID,
				name: result.name,
				description: result.description
			};
			user_result.enrollments.push(newEnrollment);

			return user_result.save().then((result, error) => {
				if(error){
					return response.send(error);
				}else{
					return true;
				}
			}).catch(err => response.send(err));
		});
	});

	if(!isUserUpdated){
		return response.send({
			message: isUserUpdated
		});
	}

	let isCourseUpdated = await Course.findById(request.body.courseID).then((course_result, error) => {
		let enrollee = {
			userId: request.user.id
		}

		course_result.enrollees.push(enrollee);

		return course_result.save().then((result, error) => {
			if(error){
				return response.send(error);
			}else{
				return true;
			}
		}).catch(err => response.send(err));
	});

	if(!isCourseUpdated){
		return response.send({
			message: isCourseUpdated
		});
	}

	if(isUserUpdated && isCourseUpdated){
		response.send(`You are now enrolled to courseID ${request.body.courseID}`);
	}
};

*/

/*
//Optional Controller if you want to check if course is already registered to the user to avoid duplicate entry.
module.exports.checkIfUserIsAlreadyEnrolledToCourse = (request, response, next) => {
	return User.findById(request.user.id).then((result, error) => {
		let isAlreadyEnrolled = false;
		for(let index = 0 ; index < result.enrollments.length; index++){
			if(result.enrollments[index].courseId === request.body.courseID){
				isAlreadyEnrolled = true;
				break;
			}
		}
		if(isAlreadyEnrolled){
			return response.send(`Course ID ${request.body.courseID} is already enrolled to ${request.user.id}`);
		}else{
			next();
		}
	});
};
*/
