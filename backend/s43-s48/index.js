// [SECTION] Dependencies and Modules
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

require('dotenv').config();

const userRoutes = require('./routes/user.js') // Allows access to routes defined within our application

const courseRoutes = require('./routes/course.js')

// [SECTION] Environment Setup
const port = 4000;

// [SECTION] Server setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

// Allows all resources to access our backend application
app.use(cors()); 

// [SECTION] Database Connection
mongoose.connect('mongodb+srv://admin:admin123@cluster0.bmmr58a.mongodb.net/S43-S48?retryWrites=true&w=majority', {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));

// [SECTION] Backend Routes
app.use('/users', userRoutes);

// Back end routes for course
app.use('/courses', courseRoutes);        //MY ANSWER --> app.use('/courses', require('./routes/course'));


// [SECTION] Server Gateway Response
if(require.main === module){
    app.listen(process.env.PORT || port, () => {
        console.log(`API is now online on port ${ process.env.PORT || port}`);
    });
};

module.exports = app;